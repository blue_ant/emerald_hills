$.validator.addMethod(
    "regex",
    function(value, element, regexp) {
        var re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
    },
    "Поле заполнено не верно!"
);

function setInputFillState(inp) {
    if (inp.value !== "") {
        inp.classList.add("active");
    } else {
        inp.classList.remove("active");
    }
}

$(function() {

    $(".view_all").on('click', function(event) {
        event.preventDefault();
        var $link = $(event.currentTarget);
        var $hiddenBlock = $link.prev(".tab_hidden_items");
        $link.fadeOut();
        $hiddenBlock.slideDown();
    });

    $('.anchscrll').on('click', function(event) {
        event.preventDefault();
        var sc = $(this).attr("href"),
            dn = $(sc).offset().top;
        $('html, body').animate({ scrollTop: dn }, 1000);
    });

    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        slidesPerView: 2,
        centeredSlides: true,
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        loop: true,
    });

    /*Ps.initialize(document.getElementById('scrollbar'));
    Ps.initialize(document.getElementById('form_order'));*/


    $("input[type='tel']").inputmask({ mask: "+7 (999) 999-99-99", showMaskOnHover: false });
    $("#orderForm1").validate({
        errorPlacement: function(error, element) {
            /*error.insertAfter(element); */
            return false;
        },
    });
    $('.form-input').each(function(index, el) {
        setInputFillState(el);
    }).on('input', function(event) {
        setInputFillState(event.currentTarget);
    });

    var COORDS = {
        lat: 55.557198,
        lng: 37.555824,
    };


    window.map = new GMaps({
        div: '#map',
        lat: COORDS.lat,
        lng: COORDS.lng,
        mapTypeControl: false,
        streetViewControl: false,
        rotateControl: false,

    });

    map.addMarker({
        lat: COORDS.lat,
        lng: COORDS.lng,
        icon: "/img/markers/building.svg",
        infoWindow: {
            content: "<b>ЖK «Эталон-Сити» &nbsp;</b>"
        }
    });

    $("[data-map-markers]").each(function(index, el) {
        var datas = JSON.parse(el.dataset.mapMarkers);
        $.each(datas, function(index, data) {
            data.details = { id: el.id };
            data.visible = false;
            map.addMarker(data);
        });

    });

    $("#accordion").on('show.bs.collapse', function(event) {

        var item = event.target;
        if (!item.dataset.mapMarkers) return;
        map.hideInfoWindows();
        map.cleanRoute();

        for (var i = 1; i < map.markers.length; i++) {
            var marker = map.markers[i];
            var isVisible = marker.details.id === item.id ? true : false;
            marker.setVisible(isVisible);
        }

        if (item.dataset.mapRoutes) {
            var routes = JSON.parse(item.dataset.mapRoutes);
            $.each(routes, function(index, routeData) {
                map.drawRoute({
                    destination: [
                        COORDS.lat,
                        COORDS.lng,
                    ],
                    origin: [
                        routeData.origin.lat,
                        routeData.origin.lng
                    ],
                    travelMode: routeData.travelMode,
                    strokeColor: routeData.color,
                    strokeOpacity: 0.8,
                    strokeWeight: 4,
                });

                if (routeData.fitLatLngBounds) {
                    map.fitLatLngBounds(routeData.fitLatLngBounds);

                } else {
                    map.fitZoom();
                }
            });

        } else {
            map.fitZoom();
        }
        if (device.desktop()) {
            map.panBy(-410, 0);
            map.setZoom(map.getZoom() - 1);
        }
    });

    $("#locationSectionTabs >li > a").on('shown.bs.tab', function(event) {

        if ($(event.target).parent().index() === 0) { //if map tab is open
            map.fitZoom();
        }
    });

    $(".view_map").on('click', function(event) {
        event.preventDefault();
        $('a[href=\'#mapWrap\']:first').trigger('click');
    });

    Ps.initialize(document.getElementById('scrollbar'));


    
    $('.b-actime__timer').each(function(index, el) {
        var $el = $(el);
        var $digitsWrap = $el.find('.b-actime__digits-wrap:first');
        var dateUntil = new Date($el.data('year'), $el.data('month') - 1, $el.data('date'), 23, 59, 59, 0);
        $(el)
            .countdown(dateUntil, {
                elapse: false,
            })
            .on('update.countdown', function(event) {
                var digitsStr = event.strftime('%D%H%M');
                var markup = "";
                for (var i = 0; i < digitsStr.length; i++) {
                    if (i === 0) {}
                    markup += '<div class="b-actime__digit">' + digitsStr[i] + '</div>';
                }

                $digitsWrap.html(markup);
            });
    });

});